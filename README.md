# Bayshann

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.2.

## Getting Started

1. Run `npm install` to install dependencies.  
2. Ensure MongoDB is up and running. Change the URI in the server/config.js file if you need to.
3. Run `npm run build` to build the front-end and start the express server


## Rest API

To view all courses: `http://localhost:3000/api/courses`  
To view students and course details of a specific course: `http://localhost:3000/api/courses/${courseId}

In dev mode, the DB is loaded with sample data.
