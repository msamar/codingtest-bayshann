import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Course } from './course.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable()
export class CourseService {
  private uri = '/api';

  constructor(private http: HttpClient) { }

  getCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(`${this.uri}/courses`);
  }

  getStudentsForCourse(id: string): Observable<any> {
    return this.http.get<Course>(`${this.uri}/courses/${id}`);
  }
}
