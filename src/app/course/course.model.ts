export class Course {
  readonly id: string;
  readonly students: string[];

  constructor(course) {
    this.id = course.id;
    this.students = course.students;
  }
}
