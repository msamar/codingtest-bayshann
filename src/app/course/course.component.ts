import { Component, OnInit } from '@angular/core';
import { CourseService } from './course.service';
import { Course } from './course.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {

  courses: Course[] = [];
  selectedCourse: Course;

  constructor(private courseService: CourseService) {
  }

  ngOnInit(): void {
    this.courseService.getCourses().subscribe(c => this.courses = c);
  }

  selectCourse({ id }) {
    this.courseService.getStudentsForCourse(id).subscribe(course => this.selectedCourse = course);
  }

  deselectCourse() {
    this.selectedCourse = null;
  }
}
