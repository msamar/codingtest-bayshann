import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseComponent } from './course.component';
import { CourseService } from './course.service';
import { Route, RouterModule } from '@angular/router';

import { MatListModule } from '@angular/material/list';

const routes: Route[] = [
  {
    path: '',
    component: CourseComponent
  }
];

@NgModule({
  declarations: [CourseComponent],
  imports: [ MatListModule, CommonModule, RouterModule.forChild(routes) ],
  exports: [RouterModule],
  providers: [CourseService],
})
export class CourseModule {}
