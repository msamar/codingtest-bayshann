
const mongoose = require('mongoose')
const config = require('./config');
const db = mongoose.connection;

db.on('error', () => console.log(`Error Connecting to the Mongodb Database at URL : ${config.DB_URI}`));

db.once('open', () => {
  db.dropDatabase();
  // Ingest dummy data on dev mode
  if (config.NODE_ENV !== 'production') {
    console.log('Loading dummy data');

    const output = require('./output.json');
    const transformedData = require('./seed')(output)
    const Course = require('./course/course.model');

    Course.insertMany(transformedData, error => {
      if (!error) {
        console.log('dummy data loaded successfully');
      } else {
        throw error('FAILED', error);
      }
    });
  }
});

mongoose.connect(config.DB_URI, { useNewUrlParser: true })
