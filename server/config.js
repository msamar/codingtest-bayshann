module.exports = {
  DB_URI: 'mongodb://127.0.0.1:27017/v1',
  NODE_ENV: process.env.NODE_ENV || 'dev'
}