const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CourseSchema = new Schema({
  id: { 
    type: String, 
    unique: true 
  },
  description: String,
  students: {
    type: [String]
  }
}, { versionKey: false });

module.exports = mongoose.model('Course', CourseSchema);
