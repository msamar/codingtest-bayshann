const Course = require('./course.model');

function getCourses(req, resp) {
  Course.find({}, '-_id -students')
  .exec((error, docs) => error ? resp.sendStatus(500) : resp.json(docs))
}

function getCourseById(req, resp) {
  const { id } = req.params;

  Course.findOne({ id: id }, '-_id', (error, doc) => {
    if (error) {
      res.status(500).send(error)
    }

    resp.status(201).send(doc);
  })
}

module.exports = {
  getCourses,
  getCourseById
}
