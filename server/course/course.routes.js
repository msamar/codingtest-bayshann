const express = require('express');
const CourseController = require('./course.controller');
const router = express.Router();

router.get('/api/courses', CourseController.getCourses);
router.get('/api/courses/:id', CourseController.getCourseById);

module.exports = router;
