
const injestData = students => {
  let courses = [];

  for (let student of students) {
    const classDetails = student.class_details;
    for (const c of classDetails) {
      courses = [...courses, {
        subject_desc: c.subject_desc,
        subject_code: c.subject_code,
        student_id: student.student_id
      }];
    }
  }

  const groupedByCode = groupBy(courses, 'subject_code');

  return generateCoursesArr(groupedByCode);
};

const groupBy = (xs, key) => {
  return xs.reduce((rv, x) => {
    (rv[x[key]] = rv[x[key]] || { students: [], description: '' });

    rv[x[key]].students.push(x.student_id)
    rv[x[key]].description = x.subject_desc;

    return rv
  }, {});
};

const generateCoursesArr = courses => {
  const arr = [];

  for (const course in courses) {

    if (courses[course]) {
      const courseObj = courses[course];
        arr.push({
          id: course,
          ...courseObj,
          students: courseObj.students.filter((item, pos) => {
            return courseObj.students.indexOf(item) == pos;
        })
      })
    }
  }

  return arr;
}

module.exports = data => injestData(data);